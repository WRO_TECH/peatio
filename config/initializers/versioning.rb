# encoding: UTF-8
# frozen_string_literal: true
# This file is auto-generated from the current state of VCS.
# Instead of editing this file, please use bin/gendocs.

module Peatio
  class Application
    GIT_TAG =    '2.2.9'
    GIT_SHA =    '2808da0'
    BUILD_DATE = '2019-06-17 13:20:47+00:00'
    VERSION =    GIT_TAG
  end
end
